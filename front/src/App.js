import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import Blogs from './Pages/Blogs';
import Educators from './Pages/Educators';
import Membersdash from './Pages/Membersdash';
import Students from './Pages/Students';
import Home from './Pages/Home';
import Memberspage from './Pages/Memberspage';
import {Switch,Route, withRouter} from 'react-router-dom';
import { Redirect } from 'react-router';
import {  makeRequestUrl } from "./uutils.js";
import { ToastContainer, toast } from "react-toastify";
import { Link } from 'react-router-dom'


const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);




// import Students from "./Pages/Students";
//import * as auth0Client from './auth';
class App extends Component {
  state={
    token: localStorage.getItem('token'),
    nick: localStorage.getItem('nick')
  }
  signedInForm=()=>
  {
    return(
    <div>
      <button onClick={() => {localStorage.removeItem('token');this.props.history.push({pathname:'/login'});}}>Log out</button>

<Link to="./members/membersdash">
   <button type="button">
        Membersdash
  </button>
</Link>
<Link to="./Students">
   <button type="button">
        Students
  </button>
</Link>
<Link to="./Students/createStudents">
   <button type="button">
        Create Students
  </button>
</Link>

<Link to="./blogs">
   <button type="button">
        blogs
  </button>
</Link>
<Link to="./blogs/createBlog">
   <button type="button">
        Create blog
  </button>
</Link>

<Link to="./Memberspage.js">
   <button type="button">
        Members
  </button>
</Link>
    </div>);
  }
  signInForm=()=>
  {
    return(
    <form className="third" onSubmit={this.onLoginSubmit}>
        <input name="username" placeholder="username" type="text" />
        <input name="password" placeholder="password" type="password" />
        <input type="submit" value="ok" />
        

      </form>
    );
  }
  loginForm = () => {
    return (
      <div>
    {  !localStorage.getItem('token')?
      
        (this.signInForm()):
      
      
       ( this.signedInForm())
      }
      <ToastContainer />
</div>
      

      
    
      
      

    );
  }
  onLoginSubmit = evt => {
    evt.preventDefault();
    const username = evt.target.username.value;
    const password = evt.target.password.value;
    if (!username) {
      toast.error("username can't be empty");
      return;
    }
    if (!password) {
      toast.error("password can't be empty");
      return;
    }
    this.login(username, password);
    
  };

  // login logout 
login = async (username, password) => {
  try {
    const url = makeUrl(`login`, { username, password, token: localStorage.getItem('token') });
    const response = await fetch(url);
    const answer = await response.json();
    if (answer.success) {
      const { token, nick } = answer.result
      this.setState({ token, nick });
      localStorage.setItem('token',token)
      localStorage.setItem('nick',nick)
      //return <Redirect to={{pathname:'/members/membersdash', state:{token:this.state.token}}} />;
      //return <Redirect to={} />;
      this.props.history.push({pathname:'/login'});
      //toast(`successful login`);
      
    } else {
      this.setState({ error_message: answer.message });
      toast.error(answer.message);
    }
  } catch (err) {
    this.setState({ error_message: err.message });
    toast.error(err.message);
  }
  
};

logout = async token => {
  try {
    const url = makeUrl(`logout`, { token: localStorage.getItem('token') });
    const response = await fetch(url);
    const answer = await response.json();
    console.log("here")
    if (answer.success) {
      
      this.setState({ token:null, nick:null });
      localStorage.removeItem('token')
      this.props.history.push({pathname:'/login'});
      //toast(`successful logout`);
      
    } else {
      this.setState({ error_message: answer.message });
      toast.error(answer.message);
    }
  } catch (err) {
    this.setState({ error_message: err.message });
    toast.error(err.message);
  }
};
// render ={()=> <Membersdash state={this.state} />}
  
 render() {
   console.log("token:",localStorage.getItem('token'))
    return (
      <div className="App">
  
    <Switch>
    <Route exact path="/" component={Home}/>
    <Route exact path="/login" render={this.loginForm}/>
    <Route exact path="/blogs" render={(props) => <Blogs {...props} token={localStorage.getItem('token')} />}/>
    <Route exact path="/blogs/createBlog" component={Blogs} token={localStorage.getItem('token')}/>
    <Route exact path="/educators" component={Educators} />
    <Route exact path="/members/membersdash" render ={()=> ( localStorage.getItem('token') ? (
      <Membersdash/>
      ) : (
      <Redirect to ="/login"/>

    )
    )}/>
    
    
    {/* // ) <Membersdash token={this.state.token} />}/> */}
    <Route exact path="/students" render={(props) => <Students {...props} token={localStorage.getItem('token')} />} />
    <Route exact path="/students/createStudent" component={Students} />
    <Route exact path="/Members" component={Memberspage} />
      
    </Switch>
      </div>
    );
  }
}


export default withRouter(App);
