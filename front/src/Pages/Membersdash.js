import React, { Component } from 'react';
import Memberdash from '../Components/Memberdash';
import '../styles/members.css';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { pause, makeRequestUrl } from "../uutils.js";
import "../App.css";

const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);
  

class Members extends Component { 
  state={
    members_list:[],
    error_message:"",
    name:"",
    position:"",
    image:"",
    isLoading: false,
    token:null,
    nick:null,
    redirect: false
  

  };

  getmember = async id => {
    // check if we already have the member
    const previous_member = this.state.members_list.find(
      member => member.id === id
    );
    if (previous_member) {
      return; // do nothing, no need to reload a member we already have
    }
    try {
      const response = await fetch(`http://localhost:8080/members/get/${id}`);
      const answer = await response.json();
      if (answer) {
        // add the user to the current list of members
        const member = answer.result;
        const members_list = [...this.state.members_list, member];
        this.setState({ members_list });
        toast(`contact loaded`);

      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message); // <--- show a toaster

    }
  };

  deletemember = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/members/delete/${id}`
      );
      const answer = await response.json();
      if (answer) {
        // remove the user from the current list of users
        const members_list = this.state.members_list.filter(
          member => member.member_id !== id
        );
        this.setState({ members_list });
        toast(`member deleted`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message); // <--- show a toaster
    }
  };



  updatemember = async (id, props) => {
    try {
      if (!props || !(props.name || props.position || props.image)) {
        throw new Error(
          `you need at least name or position properties to update a member`
        );
      }
      const url = makeUrl(`members/update/${id}`, {
        name: props.name,
        position: props.position,
      });

      let body = null;

      if(props.image){
        body = new FormData();
        body.append(`image`, props.image)
      }        

      const response = await fetch(url, {
        method:'POST', 
        body });
      const answer = await response.json();
      if (answer.success) {
        // we update the user, to reproduce the database changes:
        const members_list = this.state.members_list.map(member => {
          // if this is the contact we need to change, update it. This will apply to exactly
          // one contact
          if (member.id === id) {
            const new_member = {
              id: member.id,
              name: props.name || member.name,
              position: props.position || member.position
            };
            toast(`member "${new_member.name}" updated`);
            return new_member;
          }
          // otherwise, don't change the contact at all
          else {
            return member;
          }
        });
        this.setState({ members_list });
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };

  createmember = async props => {
    try {
      if (!props || !(props.name && props.position && props.image)) {
        throw new Error(
          `you need name and position and image to create a new member`
        );
      }
      const {name, position, image} = props;
      let body = null;

      if(image){
        body = new FormData();
        body.append(`image`, image)
      }
  
      console.log(name, position, image);
      const response = await fetch(
        `http://localhost:8080/members/new?name=${name}&position=${position}`,{
        method:'POST',
        body
      });
  
      console.log(response)
      const answer = await response.json();

      if (answer) {
       // we reproduce the user that was created in the database, locally
       const id = answer.result;
       const member = { name, position, image, id};
       console.log(member)
       const members_list = [...this.state.members_list, member]
       this.setState({ members_list });
       toast(`member "${name}" created`);
      } else {
        this.setState({error_message: answer.message })
        toast.error(answer.message);
      }
    } catch (err) {
    this.setState({ error_message: err.message });
    toast.error(err.message);

  }
 };

getmemberslist = async order => {
  this.setState({ isLoading: true });
  try {
    const url = makeUrl(`members/list`, {order, token: this.state.token})
    const response = await fetch(url);
    

    await pause()

    const answer = await response.json();
    if (answer) {
      const members_list = answer.result;
      this.setState({ members_list, isLoading: false });
      toast("members loaded"); // <--- Show a toaster

    } else {
      this.setState({ error_message: answer.message, isLoading: false });
      toast.error(answer.message); // <--- show a toaster

    }
  } catch (err) {
    this.setState({ error_message: err.message, isLoading: false });
    toast.error(err.message); // <--- show a toaster

  }
};






  componentDidMount() {
    this.getmemberslist();
  }
  onSubmit = (evt) => {
    // stop the form from submitting:
    evt.preventDefault();
    const image = evt.target.fileField.files[0]

    // extract name and position and image from state
    const { name, position } = this.state;
    // create the member form name and position and image
    console.log(name, position, image)
    this.createmember({ name, position, image });
    // empty name and position and image so the text input fields are reset
    this.setState({ name: "", position: "", image: "" });
  };
  
  
  renderUserLoggedIn() {
    const { nick } = this.state;
    return (
      <div>
        Hello, {nick}! <button onClick={this.logout}>logout</button>
        {this.renderdashboared()}
      </div>
    );
  }

renderdashboared=() =>{
  const { members_list, error_message, isLoading } = this.state

  return (
    <div className="App">
    
      {isLoading ? (
        <p>loading...</p>
      ) : (
        <>
        {members_list.map(member => (
          <Memberdash
            key={member.member_id}
            id={member.member_id}
            name={member.name}
            position={member.position}
            image={member.image}
            updatemember={this.updatemember}
            deletemember={this.deletemember}
          />
        ))}
        </>
    )}
      
      <form className="third" onSubmit={this.onSubmit}>
        <input
          type="text"
          placeholder="name"
          onChange={evt => this.setState({ name: evt.target.value })}
          value={this.state.name}
        />
        <input
          type="text"
          placeholder="position"
          onChange={evt => this.setState({ position: evt.target.value })}
          value={this.state.position}
        />
        <input
          type="file"
          placeholder="image"
          onChange={evt => this.setState({ image: evt.target.value })}
          value={this.state.image}
          name="fileField"
        />

        <div>
          <input type="submit" value="ok" />
          <input type="reset" value="cancel" className="button" />
        </div>
      </form>
      <ToastContainer />


      
    </div>
  );
}


  render() {
    return(
      
      this.renderUserLoggedIn()
    );
  }
}

 export default Members;