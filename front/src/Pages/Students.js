import React, { Component } from 'react';
import '../App.css';
import { withRouter, Route, Switch, Link } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import '../styles/students.css';
//import Student from '../Components/Student';
import PatNavbar from '../Components/PatNavbar.js';
import { pause, makeRequestUrl } from "./utils.js";
import { Redirect } from "react-router-dom";
import Memberview from '../Components/MembersView';
import Membersfooter from '../Components/Membersfooter';



const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);

class Students extends Component {

  state = {
    students_list: [],
    error_message: "",
    name: "",
    //image: "",
    level: "",
    dob: "",
    token: null,
    nick: null,
    isLoading: false
  };

  getAllStudents = async order => {
    this.setState({ isLoading: true });
    const url = makeUrl(`students/list`, { order, token: this.state.token });
    const response = await fetch(url);
    await pause();
    const answer = await response.json()
    if (answer) {
      const students_list = answer.result
      this.setState({ students_list })
    }
  }

  addStudents = async (props) => {
    try {
      if (!props || !(props.name && props.image && props.level && props.dob)) {
        throw new Error(
          `you need name and position and image to create a new member`
        );
      }
      const {name, image, level, dob} = props;
      let body = null;
      if(image){
        body = new FormData();
        body.append(`image`, image)
      }
      const url = makeUrl(`students/add`, {
        name, level, dob,
        token: this.state.token
      });
      console.log(url);
      const response = await fetch(url,{
        method:'POST',
        body
      });
  
    const answer = await response.json();
    console.log(answer)
    if (answer) {
      const id = answer.result;
       const student = { name, image, level, dob, id};
       console.log(student)
       const students_list = [...this.state.students_list, student]
       this.setState({ students_list });
       toast(`student "${name}" created`);
      } else {
        this.setState({error_message: answer.message })
        toast.error(answer.message);
      }
    } catch (err) {
    this.setState({ error_message: err.message });
    toast.error(err.message);

  }
 };

  deleteStudents = async id => {
    const url = makeUrl(`students/delete/${id}`, { token: this.state.token });
    const response = await fetch(url);
    const answer = await response.json();
    if (answer) {
      const students_list = this.state.students_list.filter(
        student => student.student_id !== id
      );
      this.setState({ students_list });
    }
  }

  

  updateStudents = async (id, props) => {
    try {
      if (!props || !(props.name || props.dob || props.level || props.image)) {
        throw new Error(
          `you need at least name or position properties to update a member`
        );
      }
      const url = makeUrl(`students/update/${id}`, {
        name: props.name,
        dob: props.dob,
        level: props.level
      });

      let body = null;

      if(props.image){
        body = new FormData();
        body.append(`image`, props.image)
      }        

      const response = await fetch(url, {
        method:'POST', 
        body });
      const answer = await response.json();
      if (answer.success) {
      const students_list = this.state.students_list.map(student => {
        if (student.student_id === id) {

          const updatedStudents = {
            id: student.student_id,
            name:props.name || student.name,
            level:props.level || student.level,
            dob:props.dob || student.dob
          };
          toast(`student "${updatedStudents.name}" updated`);
            return updatedStudents;
        }
      else {
         return student
        }
      });
      this.setState({ students_list });
    } else {
      this.setState({ error_message: answer.message });
      toast.error(answer.message);
    }
  } catch (err) {
    this.setState({ error_message: err.message });
    toast.error(err.message);
  }
};
  login = async (username, password) => {
    try {
      const url = makeUrl(`login`, {
        username,
        password,
        token: this.state.token
      });
      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {
        const { token, nick } = answer.result;
        this.setState({ token, nick });
        toast(`successful login`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };
  logout = async token => {
    try {
      const url = makeUrl(`logout`, { token: this.state.token });
      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {
        this.setState({ token: null, nick: null });
        toast(`successful logout`);
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };

  componentDidMount() {
    this.getAllStudents()
  }


  render() {
  
    return (
<div>
  
    <header>


   <PatNavbar />
 
  
  </header>
                <div className="membercardshow">
                <div className="container"></div>
    <div className="at-section">
      <div className="at-section__title">Our Students</div>
    </div>
    <div className="at-grid" data-column="3">
          {this.state.students_list.map(student => (
  
            <Memberview
              key={student.student_id}
              id={student.student_id}
              name={student.name}
              level={student.level}
              dob={student.dob}
              image={student.image}
            />
  ))}
         
  </div>
        </div>
        
        <footer>
  <Membersfooter />     </footer>
        </div>
    )
          }

  onSubmit=(e)=>{
    e.preventDefault()

  //if (e.target.fileField.files[0])
  const image = e.target.fileField.files[0];
    const{name,level,dob} = this.state
    this.addStudents({name,level,dob,image})
    this.setState({name:"",level:"",dob:""})
  };
  
  /** onLoginSubmit = (e) => {
    e.preventDefault();
    const username = e.target.username.value
    const password = e.target.password.value
    if (!username) {
      toast.error("username can't be empty");
      return
    }
    if (!password) {
      toast.error("password can't be empty");
      return
    }
    this.login(username, password)
  };*/
      
  renderUser() {
    const { token } = this.state
    if (token) { // user is logged in
      return this.renderUserLoggedIn()
    } else {
      return this.renderUserLoggedOut()
    }
  };

  renderUserLoggedOut() {
    return (<form className="third" onSubmit={this.onLoginSubmit}>
      <input name="username" placeholder="username" type="text" />
      <input name="password" placeholder="password" type="password" />
      <input type="submit" value="ok" />
    </form>)
  };

  renderUserLoggedIn() {
    const { nick } = this.state
    return (<div>Hello, {nick}! <button onClick={this.logout}>logout</button></div>)
  };

 /* renderStudentsPage = () => {
    const { students_list } = this.state;
    return (<div>{students_list.map(student =>
      <Student
        key={student.student_id}
        id={student.student_id}
        name={student.name}
        level={student.level}
        image={student.image}
        dob={student.dob}
        updateStudents={this.updateStudents}
        deleteStudents={this.deleteStudents}
        // token={this.state.token}
      />
    )
    }
</div>
    )
}**/
      renderCreateForm = () => { //const {AllStudents,image} = this.state;
        return (
          
          <form
            className="thirdm"
            onSubmit={this.onSubmit}
          >
            <input
              type="text"
              placeholder="name"
              onChange={e => this.setState({ name: e.target.value })}
              value={this.state.name}
              className="put"
            /> <br/>
            <input
              type="text"
              placeholder="level"
              onChange={e => this.setState({ level: e.target.value })}
              value={this.state.level}
              className="put"
            /><br/>
            <input
              type="text"
              placeholder="dob"
              onChange={e => this.setState({ dob: e.target.value })}
              value={this.state.dob}
              className="put"
            /><br/>
               <input
          type="file"
          placeholder="image"
          onChange={evt => this.setState({ image: evt.target.value })}
          value={this.state.image}
          name="fileField"
          className="put"
        />
            <div>
              <input type="submit" value="ok" className="mit"/>
              <input type="reset" value="cancel" className="mit" />
            </div>
          </form>
      
        );
      }
    }
      
       
      
    
    
    

    export default withRouter(Students);