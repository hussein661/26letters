import React, { Component } from 'react';
import '../styles/educators.css'
import Educator from '../Components/Educator';
import HusseinNavbar from '../Components/HusseinNavbar';

class Educators extends Component {

  state = {
    allEducators: [],
    name: "",

  }

  getAllEducators = async () => {
    const response = await fetch('http://localhost:8080/edu/list')
    const answer = await response.json()
    if (answer) {
      const allEducators = answer.result
      this.setState({ allEducators })
    }
  }

  addEducator = async (name, about, image) => {
    let body = null;

    if(image){
      body = new FormData();
      body.append(`image`, image)
    }

    const response = await fetch(`http://localhost:8080/edu/add?name=${name}&about=${about}&image=${image}`,{
      method:'POST',
      body
    });

    const answer = await response.json();
    if (answer) {
      const id = answer.result;
      const educator = { name, about, image, id }
      const allEducators = [...this.state.allEducators, educator]
      this.setState({ allEducators })

    }
  }


  deleteEducator = async id => {
    const response = await fetch(`http://localhost:8080/edu/delete/${id}`);
    const answer = await response.json();
    if (answer) {
      const allEducators = this.state.allEducators.filter(
        educator => educator.educator_id !== id
      );
      this.setState({ allEducators });
    }
  }



  updateEducator = async (id, { name, about, image }) => {
    const response = await fetch(`http://localhost:8080/edu/update/${id}?name=${name}&about=${about}&image=${image}`)
    const answer = await response.json();
    if (answer) {
      const allEducators = this.state.allEducators.map(educator => {
        if (educator.educator_id === id) {

          const updatedEducator = {
            id: educator.educator_id,
            name,
            about,
            image: educator.image,
          }
          return updatedEducator
        }
        else {
          return educator
        }
      });
      this.setState({ allEducators });
    }
  }

  componentDidMount() {
    this.getAllEducators()
  }

  onSubmit = (e) => {
    e.preventDefault()

    const image = e.target.fileField.files[0]
    this.addEducator(this.state.name, this.state.about, image)
    this.setState({ name: "", about: "" })
  }



  render() {
    const { allEducators } = this.state
    return (
      <div>
        <div className="all-cardedus">
          {/*form add starts here*/}
          <form onSubmit={this.onSubmit} className="addform">
            <label for="name">name</label>
            <input className="form-control"
              type="text"
              onChange={event => this.setState({ name: event.target.value })}
              value={this.state.name}
            />

            <label for="about">about</label>
            <input className="form-control"
              type="text"
              onChange={event => this.setState({ about: event.target.value })}
              value={this.state.about}
            />

            <label for="image">image</label>
            <input className="form-control"
              type="file" name="fileField"

            />

            <input className="form-btn" type="submit" value="add" />
          </form>
          {/*form add ends here*/}

        </div>
        <div className="all-single-cards">
          {allEducators.map(edu =>
            <Educator
              key={edu.educator_id}
              id={edu.educator_id}
              name={edu.name}
              about={edu.about}
              image={edu.image}
              updateEducator={this.updateEducator}
              deleteEducator={this.deleteEducator}
            />
          )}
        </div>
      </div>
    );
  }
}

export default Educators;