import React, { Component } from 'react';
import { toast } from "react-toastify";
import { Link } from 'react-router'
import { fork } from 'child_process';


class loginpage extends Component {



  onLoginSubmit = evt => {
    evt.preventDefault();
    const username = evt.target.username.value;
    const password = evt.target.password.value;
    if (!username) {
      toast.error("username can't be empty");
      return;
    }
    if (!password) {
      toast.error("password can't be empty");
      return;
    }
    this.login(username, password);
  };
  render() {
    return (
      <div>

      <form className="third" onSubmit={this.onLoginSubmit}>
        <input name="username" placeholder="username" type="text" />
        <input name="password" placeholder="password" type="password" />
        <input type="submit" value="ok" />
        <button onClick={() => localStorage.setItem('token', null)}>Log out</button>
      </form>
      <div>
        <form>
      <Link to="./Membersdash.js">
         <button type="button">
              Membersdash
        </button>
      </Link>
      <Link to="./Students.js">
         <button type="button">
              Students
        </button>
      </Link>
      <Link to="./blogs.js">
         <button type="button">
              blogs
        </button>
      </Link>
      <Link to="./Memberspage.js">
         <button type="button">
              Members
        </button>
      </Link>
      </form>
</div>

    );

        
      </div>
    );
  }
}

export default loginpage;