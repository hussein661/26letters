import React, { Component } from "react";
import Blog   from '../Components/Blog';
import '../styles/blogs.css';
import PatNavbar from '../Components/PatNavbar.js';
//import '../styles/home.css'

import {  ToastContainer,toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { makeRequestUrl } from "./utils.js";
import {withRouter, Switch, Link, Route} from 'react-router-dom';
import { Redirect } from 'react-router';


const makeUrl = (path, params) =>
makeRequestUrl(`http://localhost:8080/${path}`, params);


class Blogs extends Component {

state = {
  allBlogs: [],
  // images: [],
  title: "",
  description: "",
  // image: "",
  content: "",
  date: "",
  image:"",
  token: null,
    nick: null
}

getAllBlog = async () => { 
  
  const url = makeUrl('blog/list', {token:this.state.token});
  console.log("hello", url)
  const response = await fetch(url)
 
  const answer  = await response.json() 
  if (answer){
  const allBlogs = answer.result
  this.setState({ allBlogs })
            }
}

addBlog = async (title, description, image, content, date) => {
  try {
  let body = null;
  if(image) {
    body = new FormData();
    body.append(`image`, image)

  }
  console.log(body);
  const url = makeUrl(`/blog/add`, {
    title, description, image, content, date,
    token: localStorage.getItem('token')
  });

  const response = await fetch(url,{
    method: 'POST', body
  });
  console.log(response)
  const answer = await response.json();
  if (answer) {
    const id = answer.result;
    const blog = { title, description, image, content, date, id }
    const allBlogs = [...this.state.allBlogs, blog]
    this.setState({ allBlogs })

  } else {
    console.log("DEBUG", answer);
  }
  } catch (e){ console.log(e)}
}

deleteBlog = async id => {
const url = makeUrl(`/blog/delete/${id}` , { token: localStorage.getItem('token') });
const response = await fetch(url);
const answer = await response.json();
if (answer) {
const allBlogs = this.state.allBlogs.filter(
        blog => blog.blog_id !== id
      );
      this.setState({ allBlogs });
    } 
};


updateBlog = async (id, { title, description, image, content, date}) => {
  const url = makeUrl(`/blog/update/${id}`, {
    title,
    description,
    image,
    content,
    date,
    token: localStorage.getItem('token')
  });

  const response = await fetch(url
);
const answer = await response.json();
if (answer) {
const allBlogs = this.state.allBlogs.map(blog => {
if (blog.blog_id === id) {
        const updateBlog = {
        id: blog.blog_id,
        title:  blog.title,
        description, 
        image : blog.image,
        content : blog.content,
        date : blog.date
        };
return updateBlog
}
else {
return blog;
}
});
this.setState({ allBlogs });
}  
};


//logins & Logout
login = async (username, password) => {
  try {
    const url = makeUrl(`login`, { username, password, token: localStorage.getItem('token') });
    const response = await fetch(url);
    const answer = await response.json();
    if (answer.success) {
      const { token, nick } = answer.result
      this.setState({ token, nick });
      toast(`successful login`);
    } else {
      this.setState({ error_message: answer.message });
      toast.error(answer.message);
    }
  } catch (err) {
    this.setState({ error_message: err.message });
    toast.error(err.message);
  }
};
logout = async token => {
  try {
    const url = makeUrl(`logout`, { token: localStorage.getItem('token') });
    const response = await fetch(url);
    const answer = await response.json();
    if (answer.success) {
      this.setState({ token:null, nick:null });
      toast(`successful logout`);
    } else {
      this.setState({ error_message: answer.message });
      toast.error(answer.message);
    }
  } catch (err) {
    this.setState({ error_message: err.message });
    toast.error(err.message);
  }
};


componentDidMount() {
  console.log("here")
  this.getAllBlog();
}


onSubmit = (e) => {
  e.preventDefault();

  const image = e.target.blogImage.files[0]
  this.addBlog(
    this.state.title,
     this.state.description,
     image, 
     this.state.content,
     this.state.date   );

   this.setState({ title: "", description : "",/*image: "",*/ content: "",date: ""  });
};

onLoginSubmit = evt => {
  evt.preventDefault();
  const username = evt.target.username.value;
  const password = evt.target.password.value;
  if (!username) {
    toast.error("username can't be empty");
    return;
  }
  if (!password) {
    toast.error("password can't be empty");
    return;
  }
  this.login(username, password);
};

renderUser=()=> {
  console.log("check login")
  const token  = localStorage.getItem('token');
  if (token) {
    // user is logged in
    return this.renderUserLoggedIn();
  } else {
    return this.renderUserLoggedOut();
  }
}
renderUserLoggedOut() {
  return (
    <form  onSubmit={this.onLoginSubmit}>
      <input name="username" placeholder="username" type="text" />
      <input name="password" placeholder="password" type="password" />
      <input type="submit" value="ok" />
    </form>
  );
}
renderUserLoggedIn() {
  const { nick } = this.state;
  return (
    <div>
      Hello, {nick}! <button onClick={this.logout}>logout</button>
    </div>
  );
}
renderBlogsPage = () => {
  //console.log("here2", this.props)
  const { allBlogs} = this.state;
  /* return <allBlogs allBlogs={allBlogs} />;

  const { allBlogs } = this.state; */
  /* return <allBlogs blogs={allBlogs} /> */

  return (  <div>  <header>


    <PatNavbar/>
  
   
   </header>
  
  <div className="lone-line">{ allBlogs.map(blog => 
        <Blog 
        key = {blog.blog_id}
        id = {blog.blog_id}
        title = {blog.title}
        description = {blog.description }
        image = {blog.image}
        content = {blog.content}
        date = {blog.date}
        updateBlog = {this.updateBlog}
        deleteBlog = {this.deleteBlog}
        token={localStorage.getItem('token')}
      />)}
      </div></div>
      );
    }

renderCreateForm = () =>  {
 // console.log("create")
 // const { allBlogs,image } = this.state;
  return ( 
    
      <div className="genome-blog">
        
        <form onSubmit={this.onSubmit} className="mainform">
        
        {<br />}
          <label for="title">name</label>
          <input className="form-control"
            type="text"
            onChange={event => this.setState({ title: event.target.value })}
            value={this.state.title}
            />
            {<br />}
          <label for="description">description</label>
          <input className="form-control"
            type="text"
            onChange={event => this.setState({ description: event.target.value })}
            value={this.state.description}
            />
            {<br />}
            <label for="image">image</label>
          <input className="form-control"
            type="file"
            name ="blogImage"
            />
            {<br />}
          <label for="content">content</label>
          <input className="form-control"
            type="text"
            onChange={event => this.setState({ content: event.target.value })}
            value={this.state.content}
            />
            {<br />}
            <label for="date">date</label>
          <input className="form-control"
            type="text"
            onChange={event => this.setState({ date: event.target.value })}
            value={this.state.date}
            />
            {<br />}
          <input className="form-btn" type="submit" value="add" />
        </form>
        
</div>
      
 );}

          
renderContent(){ 
  //console.log(this.props.match)
   return (
    <Switch>
      
      <Route path="/login" render={this.renderUser} />
      <Route path="/blogs" exact render={this.renderBlogsPage} />
      <Route path="/blogs/createBlog" render ={()=> (!localStorage.getItem('token') ? (
        <Redirect to ="/login"/>
      ) : (
      
      this.renderCreateForm()

    )
    )}/>
    </Switch>
)
  
   }
  render(){
    console.log("token in blogs",localStorage.getItem('token'));
  return (
   
      <div className="App">
        <div>
        
          {/* <Link to="/login">Login</Link>
          <Link to="/blogs/createBlog">createBlog</Link> */}
        </div>
        {this.renderContent()}
        <ToastContainer />
      </div>
  )
  }
}


export default withRouter(Blogs);
