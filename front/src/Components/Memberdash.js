import React, { Component } from 'react';


export default class Memberdash extends React.Component {
  state = {
    editMode: false
  };
  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };
  // renderEditMode() {
  //   const { id, name, poition, image, updatemember, deletemember } = this.props;
  
  //   return <div>Edit mode</div>
  // }


  renderViewMode() {
    const { id, name, position, image, deletemember } = this.props;
    // console.log(id)
    return (
      <div className="cardmember">
      
      { image && <img className="memimage" src={`//localhost:8080/images/${image}`  } alt={`the avatar of ${name}`}/> }
      <div className="member_info">
        <h1><b>{name}</b></h1>
        <p>{position}</p>
        <button onClick={() => deletemember(id)} >Delete</button>
        <input
              type="button"
              className="form-btn"
              value="edit"
              onClick={this.toggleEditMode} />
      </div>      
       </div>

    );
  }
  renderEditMode() {
    const { name, position, image } = this.props;
    return(
      
        
    <form
      className="third"
      onSubmit={this.onSubmit}
      onReset={this.toggleEditMode}
      >
      <input
        type="text"
        placeholder="name"
        name="member_name_input"
        defaultValue={name}
        />

      <input
      type="text"
      placeholder="postiotn"
      name="member_position"
      defaultValue={position}
      />
 
      <input
      type="file"
      placeholder="member_picture"
      name="member_picture"
      /> 
      <div>
        <input type="submit" value="ok"/>
        <input type="reset" value="cancel" className="button" />
      </div>
      </form>
      
    );
  }
  onSubmit = evt => { 
        // stop the page from refreshing
        evt.preventDefault();
        // target the form
        const form = evt.target;
       
        // extract the three inputs from the form
        const n = form.member_name_input;
        //console.log("herer" + n.value)
        const p = form.member_position;
        const i = form.member_picture;

        // extract the values
       const name = n.value;
        const position = p.value;
      const image = i.files[0];

        // get the id and the update function from the props
        const { id, updatemember } = this.props;
        // run the update member function
        updatemember(id, { name, position, image });
        // toggle back view mode
        this.toggleEditMode();
      };
      
  
  render() {
    const { editMode } = this.state;
    if (editMode) {
      return this.renderEditMode();
    } else {
      return this.renderViewMode();
    }
  }
}
