import React, { Component } from 'react';
import '../styles/Membersfooter.css';
import {Link} from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


class Membersfooter extends Component {
  render() { 
    return (

      <footer className="footer-distributed">

			<div className="footer-left">

				<h3>Company<span>logo</span></h3>

				<p className="footer-links">
				<Link to="/">Home</Link>
					-
					<Link to="/blogs">Our Blog</Link>
					-
					<Link to="/members">Members</Link>
					-
					<Link to="/about">About</Link>					
					-
				</p>

				<p className="footer-company-name">26 Letters &copy; 2019</p>
			</div>
			<div className="footer-center">

				<div>
					<i className="fa fa-map-marker"></i>
					<p><span>21 Revolution Street</span> Beirut, Lebanon</p>
				</div>

				<div>
					<i className="fa fa-phone"></i>
					<p>+961 7035226</p>
				</div>

				<div>
					<i className="fa fa-envelope"></i>
					<p><a href="mailto:support@company.com">info@26Letters.com</a></p>
				</div>

			</div>

			<div className="footer-right">

				<p className="footer-company-about">
					<span>About 26 Letters</span>
          26 Letters is much more than a project. It is an aim, an impulse, and a dream. Our aim?
          To change the reality of children who are bound by their economic, social and/or geo-
          political situation. Our impulse? To empower students through projects dedicated to
          serving the educational, vocational and personal needs of every child and teenager in
          Beirut by making free and quality education accessible for all. Our dream? To leave no
          child behind.
				</p>

				<div className="footer-icons">

					<a href="https://www.facebook.com/26letterslebanon/"><i className="fa fa-facebook"></i></a>
					<a href="#"><i className="fa fa-twitter"></i></a>
					<a href="#"><i className="fa fa-linkedin"></i></a>

				</div>

			</div>

		</footer>


    );
  }
}


    export default Membersfooter;
