import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import '../styles/home.css'

class HusseinNavbar extends Component{
    render() {
       return (
        <div>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="icon" href="img/favicon.png" type="image/png" />
        <link rel="stylesheet" href="main.css" />
        <header className="header_area">
          <div className="main_menu">
            <nav className="navbar navbar-expand-lg navbar-light">
              <div className="container">
                {/* Brand and toggle get grouped for better mobile display */}
                <a className="navbar-brand logo_h" href="homepage.html"><img src="" className="Logo" /></a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                </button>
                {/* Collect the nav links, forms, and other content for toggling */}
                <div className="collapse navbar-collapse offset" id="navbarSupportedContent">
                  <ul className="nav navbar-nav menu_nav justify-content-center">
                    <li className="nav-item active"><a className="nav-link" href="homepage.html">HOME</a></li>
                    <li className="nav-item submenu dropdown">
                      <a href="#" className="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">OUR WORK</a>
                      <ul className="dropdown-menu">
                        <li className="nav-item"><a className="nav-link dropdown-color2" href="whatdowedo.html">What do we do?</a>
                        </li><li className="nav-item"><a className="nav-link dropdown-color2" href="howdowedoit.html">How do we do it?</a>
                        </li><li className="nav-item"><a className="nav-link dropdown-color2" href="whatmakesusdifferent.html">What makes us different?</a>
                        </li><li className="nav-item"><a className="nav-link dropdown-color2" href="whydowedoit.html">Why do we do it?</a>
                        </li></ul>
                    </li>
                    <li className="nav-item submenu dropdown">
                      <a href="#" className="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">MEET THE FAMILY</a>
                      <ul className="dropdown-menu">
                        <li className="nav-item"><a className="nav-link dropdown-color2" href="founders.html">The founders</a>
                        </li><li className="nav-item"><a className="nav-link dropdown-color2" href="educators.html">The educators</a>
                        </li><li className="nav-item"><a className="nav-link dropdown-color2" href="staff.html">The staff members</a>
                        </li><li className="nav-item"><a className="nav-link dropdown-color2" href="shortvolunteers.html">The short-time volunteers</a>
                        </li><li className="nav-item"><a className="nav-link dropdown-color2" href="students.html">The students</a>       
                        </li></ul>
                    </li>
                    <li className="nav-item submenu dropdown">
                      <a href="#" className="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GET INVOLVED</a>
                      <ul className="dropdown-menu ">
                        <li className="nav-item"><a className="nav-link dropdown-color1" href="family.html">Become part of the family</a>
                        </li><li className="nav-item"><a className="nav-link dropdown-color1" href="sponsor.html">Become our sponsor</a>
                        </li><li className="nav-item"><a className="nav-link dropdown-color1" href="partner.html">Become our partner</a>
                        </li><li className="nav-item"><a className="nav-link dropdown-color1" href="Arabiclearning.html">Become an Arabic student</a>
                        </li><li className="nav-item"><a className="nav-link dropdown-color1" href="BigSibling.html">Become a Big Sibling</a>
                        </li></ul>
                    </li>
                    <li className="nav-item submenu dropdown">
                      <a href="#" className="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">NEED OUR HELP?</a>
                      <ul className="dropdown-menu">
                        <li className="nav-item"><a className="nav-link dropdown-color2" href="enroll.html">I want to enroll my kids</a></li>
                        <li className="nav-item"><a className="nav-link dropdown-color2" href="books.html">I need books and materials for my class</a></li>
                      </ul>
                    </li>
                    <li className="nav-item submenu dropdown">
                      <a href="#" className="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">NEWS</a>
                      <ul className="dropdown-menu">
                        <li className="nav-item"><a className="nav-link dropdown-color1" href="futureprojects.html">Future Projects</a></li>
                        <li className="nav-item"><a className="nav-link dropdown-color1" href="26blog.html">26 Letters' Blog</a></li>
                      </ul>
                    </li>
                    <li className="nav-item"><a className="nav-link" href="contact.html">Contact</a></li> 
                  </ul>
                 
                </div>
              </div>
            </nav>
          </div>
        </header>
      </div>
      
       )

       
       }
 }

 export default HusseinNavbar;
