import React, { Component } from 'react';

export default class Member extends React.Component {
  state = {
    editMode: false
  };
  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };
  // renderEditMode() {
  //   const { id, name, poition, image, updatemember, deletemember } = this.props;
  
  //   return <div>Edit mode</div>
  // }


  renderViewMode() {
    const { id, name, position, image } = this.props;
    return (
      <div className="at-column">

       <div className="at-user">
        <div className="at-user__avatar">
        { image && <img className="memimage" src={`//localhost:8080/images/${image}`  } alt={`the avatar of ${name}`}/> }
</div>
        <div className="at-user__name">{name}</div>
        <div className="at-user__title">{position}</div>
        
      </div>
      </div>
      
    );
  }
  render() {
      return this.renderViewMode();
    }
  }
  