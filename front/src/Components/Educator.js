import React, { Component } from 'react';

class Educator extends Component {


  state= {
    editMode :false
  }
  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };

  
  renderViewMode() {
    const {id,name,about,deleteEducator,image} = this.props
    return (
      <div class="card-edu">
        <img src={`//localhost:8080/images/${image}`} alt="Avatar" />
        <div class="sub-cardedu">
          <h1><b>{name}</b></h1>
          <p>{about}</p>
          <button onClick={() => deleteEducator(id)} >Delete</button>
          <input
                type="button"
                className="form-btn"
                value="edit"
                onClick={this.toggleEditMode} />
        </div>    
           </div>
    )
  }

  save = event => {
    const { id, updateEducator } = this.props;
    event.preventDefault();
    const form = event.target;
    const name = form.name.value;
    const about = form.about.value;
    updateEducator(id, { name, about });
    this.toggleEditMode();
  }


  renderEditMode = () => {
    const {name,about} = this.props
    return(
    <form
     onSubmit={this.save}
     className="addform">
      <label for="name">name</label>
      <input className="form-control"
        type="text"
        name="name"
        defaultValue={name}
      />
  
      <label for="about">about</label>
      <input className="form-control"
        type="text"
        name="about"
        defaultValue={about}
      />
  
      <input className="form-btn" type="submit" value="okay" />
      <input
              type="button"
              className="form-btn"
              value="cancel"
             />
    </form>
      
    )
  }
     

  render() {
    const {editMode} = this.state;
      if(editMode) {
        return this.renderEditMode()
      }else{
          return this.renderViewMode()
        }
     

  }
}


export default Educator;