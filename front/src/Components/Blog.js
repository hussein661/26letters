import  React, { Component } from 'react';

import '../styles/home.css'
//import '../style/blog.css'
class Blog extends Component {

  state = {
    editMode: false
  };


  // toggleEditMode = () => {
  //   const editMode = !this.state.editMode;
  //   this.setState({ editMode });
  // };
 
  renderViewMode() {
    const { title, description, image,content, date } = this.props
    return (
      <div className=" single-grid">
        
        <img className="blogImage" src={`//localhost:8080/images/${image}`} alt="BlogImage" />
        
        <h1><b>{title}</b></h1>
        <h1><b>{description}</b></h1>
        <h1><b>{content}</b></h1>
        <h1><b>{date}</b></h1>
        
      </div>      
      
     
    );
  }
  onGo = event => {
    const { id, updateBlog} = this.props;
    event.preventDefault();
    const form = event.target;
    const title = form.title.value;
    const description = form.description.value;
    //  const image= form.image.value;
    const image= form.image.files[0];
    // const blogImage = form.blogImage;
    // const image= blogImage.files[0];
    const content = form.content.value;
    const date = form.date.value;
    updateBlog(id, { title, description , content, image, date });
    this.toggleEditMode();
  };

  renderEditMode() {console.log("i can edit")
    const { id, title, description, /*image,*/ content, date, deleteBlog } = this.props
    
    return(
  <form className="main form" onSubmit={this.onGo} onReset={this.toggleEditMode}>   

              <label for="title">title</label>

                <input type="text" placeholder="title" name="title" defaultValue={title}/>

              <label for="description">Description</label>

                <input type="text" placeholder="description" name="description" defaultValue={description}/>

              <label  for="image">Image</label>
                
                <input type="file" multiple name="blogImage"/>
               
              <label for="content">Content</label>
                <input type="text" placeholder="content" name="content" defaultValue={content}/>

                <label for="date">Date</label>
                <input type="text" placeholder="date" name="date" defaultValue={date}/> 
                <button onClick={() => deleteBlog(id)} >Delete</button>
                <input
                      type="button"
                      className="form-btn"
                      value="edit"
                      onClick={this.toggleEditMode} />
               
  </form>
    );
  }
  

 
  render() {
    console.log("in blog",this.props.token)
    if  (!localStorage.getItem('token')){
      console.log("i want to show anyway")
      return this.renderEditMode();
    } else {
			
			return this.renderViewMode();
    }
  }
}


export default Blog;

