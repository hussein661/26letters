import React, { Component } from 'react';
import '../styles/students.css'

class Student extends Component {


  state= {
    editMode :false
  }
  toggleEditMode = () => {
    const editMode = !this.state.editMode;
    this.setState({ editMode });
  };

  
  renderViewMode() {
    const {name,image,level,dob} = this.props
    return (
      <div className="studentsDiv">
          { image && <img src={`//localhost:8080/images/${image}`}  />}
       <div className="studentstext">
         
         <h1> <b>{name}</b></h1>
          <h3>{level}</h3>
          <h3>{dob}</h3> 
      
          
          </div>
         
        </div>       
    )
  }

  keep = e => {
    const { id, updateStudents } = this.props;
    e.preventDefault();
    const form = e.target;
    const name = form.name.value;
    const level = form.level.value;
    const dob = form.dob.value;
   // const image= form.image.value;
      const image = form.image;
    //const image= image.files[0];
    updateStudents(id, { name, level, dob, image});
    this.toggleEditMode();
  }


  renderEditMode = () => {
    const {id,name,level,dob,deleteStudents } = this.props
    return(
       
    <form 
     onSubmit={this.keep}
     onReset={this.toggleEditMode}
     className="addform">
      <label for="name"> Name </label>
      <input className="form-input"
        type="text"
        name="name"
        defaultValue={name}
      />
   
       <label for="dob"> DOB </label>
      <input className="form-input"
        type="text"
        name="dob"
        defaultValue={dob}
      />
      <label for="level"> LVL </label>
      <input className="form-input"
        type="text"
        name="level"
        defaultValue={level}
      />
     <label  for="image">Image</label>
      <input type="file" multiple name="StudentImage"/>
   
      <button onClick={() => deleteStudents(id)} >Delete</button>
         
      <input
          type="button"
          className="addform"
          value="edit"
          onClick={this.toggleEditMode} />
    </form>
      
    )
  }
    
  

  render() {
   // const {editMode} = this.state;
      if(localStorage.getItem('token')) {
        return this.renderEditMode()
      }else{
        return this.renderEditMode()
        }

  }
}


export default Student;