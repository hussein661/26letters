import React, { Component } from "react";
//import { NavLink } from 'react-router-dom'
import '../styles/PatNavbar.css';
import logo from '../images/26Llogoold.jpg'
import {Link} from 'react-router-dom'


class PatNavbar extends Component {
  render() { 
    return (<header>

<ul className="navv">
<li className="li"><img className="logo" src={logo} alt="Logo"/></li>
  <li className="li"><Link className="active lia"  to="/">HOME</Link></li>
  {/* <li className="li"><Link className="lia" to="#news">OUR WORK</Link></li> */}
  <li  className="li"><Link className="lia" to="/members">MEMBERS</Link></li>
  <li  className="li"><Link className="lia" to="/blogs">BLOG </Link></li>
  <li  className="li"><Link className="lia" to="/students">STUDENTS</Link></li>
  <li  className="li"><Link className="lia" to="#about">CONTACT</Link></li>
  
</ul>
</header>
    );
  }
}

export default PatNavbar;

