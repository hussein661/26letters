import app from './app'
import initializeDatabase from './db'
import path from 'path'
import multer from 'multer'
import { authenticateUser, logout, isLoggedIn } from './mauth'



const multerStorage = multer.diskStorage({
  destination: path.join(__dirname, '../public/images'),
  filename: (req, file, cb) => {
    const { fieldname, originalname } = file
    const date = Date.now()
    // filename will be: image-1345923023436343-filename.png
    const filename = `${fieldname}-${date}-${originalname}`
    cb(null, filename)
  }
})
const upload = multer({ storage: multerStorage })

const start = async () => {

  const controller = await initializeDatabase()

  app.get("/", (req, res, next) => res.send("ok"));

  // get the blog list
  app.get('/blog/list', async (req, res) => {
    const list = await controller.getAllBlog()
    console.log(list)
    res.json({ result: list })

  })
  // // get one blog 

  app.get('/blog/get/:id', async (req, res) => {
    const { id } = req.params
    const blog = await controller.getOneBlog(id)
    res.json({ result: blog })
  })

  // make the url to add a blog
  //http://localhost:8080/blog/new?title=dsd&description=dfgdfgdf&image=kgkgk&content=jgjgj&date=hfghfgh
  app.post('/blog/add', upload.single('image'), async (req, res, next) => {
    try {
      //console.log(req.query)
      const { title, description, content, date } = req.query
      const image = req.file && req.file.filename
      // console.log( title, description, image, content, date );
      const result = await controller.newBlog({ title, description, image, content, date })
      console.log(result)
      res.json({ success: true, result })
    }
    catch (e) {
      console.log(e);
      next(e);
    }
  })

  //  delete blogpost
  app.get('/blog/delete/:blog_id', async (req, res) => {
    const { blog_id } = req.params;
    const result = await controller.deleteBlog(blog_id)
    res.json({ success: true, result })
  })

  // update blogpost
  app.get('/blog/update/:id', async (req, res) => {
    const { id } = req.params
    const { title, description, image, content, date } = req.query
    const result = await controller.updateBlog(id, { title, description, image, content, date })
    res.json({ seccess: true, result })
  })
  //Blogs Page Auth
  app.get('/login', authenticateUser)
  app.get('/logout', logout)
  app.get('/mypage', isLoggedIn, ( req, res ) => {
    const username = req.user.username
    res.send({success:true, result: 'ok, user '+username+' has access to this page'})
  })

  //EDUCATORS
  // get the educators list
  app.get('/edu/list', async (req, res) => {
    const list = await controller.getAllEdu()
    res.json({ result: list })

  })
  //   // // get one Edu
  app.get('/edu/get/:id', async (req, res) => {
    const { id } = req.params
    const oneEdu = await controller.getOneEdu(id)
    res.json({ result: oneEdu })
  })

  // add a new educator
  app.post('/edu/add', upload.single('image'), async (req, res, next) => {
    try {
      const { name, about } = req.query
      const image = req.file && req.file.filename
      const result = await controller.newEdu({ name, about, image })
      res.json({ success: true, result })
    }
    catch (e) {
      next(e);
    }
  })

  // Delete Educator
  app.get('/edu/delete/:edu_id', async (req, res) => {
    const { edu_id } = req.params;
    const result = await controller.deleteEdu(edu_id)
    res.json({ success: true, result })
  })
  // // update educator
  app.get('/edu/update/:id', async (req, res) => {
    const { id } = req.params
    const { name, about, image } = req.query
    const result = await controller.updateEdu(id, { name, about, image })
    res.json({ seccess: true, result })
  })

  //STUDENTS 
  app.get('/students/list', async (req, res) => {
    const list = await controller.getAllstudents()
    res.json({ result: list })
  })

  

  app.post('/students/add',upload.single('image'), async (req, res,next) => {
    console.log("here")
    try{
      const { name, level, dob } = req.query
    const image = req.file && req.file.filename
    console.log(image)
    const result = await controller.addStudents({ name, image, level, dob })
    res.json({ success: true, result })
  }
    catch (e) {
      next(e);
    }
  })

  app.get('/students/delete/:student_id', async (req, res, next) => {
    const { student_id } = req.params
    const result = await controller.deleteStudents(student_id)
    res.json({ success: true, result })
  })

  app.get('/students/update/:id', async (req, res, next) => {
    const { id } = req.params
    const { name, image, level, dob } = req.query
    const result = await controller.updateStudents(id, { name, image, level, dob })
    res.json({ success: true, result })
  })

  app.get('/students/get/:id', async (req, res, next) => {
    const { id } = req.params
    const students = await controller.getStudents(id)
    res.json({ success: true, result: students })
  })

  /*MEMBERS */
  app.get('/members/list', async (req, res) => {
    const list = await controller.getAllmembers()
    res.json({ result: list })
  })



  app.post('/members/new', upload.single('image'), async (req, res) => {
    const { name, position} = req.query
    const image = req.file && req.file.filename
    console.log("here ",name, position, image)
    const result = await controller.addMembers({ name, position, image })
    res.json({ success: true, result })
  })

  app.get('/members/delete/:member_id', async (req, res, next) => {
    const { member_id } = req.params
    const result = await controller.deleteMembers(member_id)
    res.json({ success: true, result })
  })

  app.post('/members/update/:id', upload.single('image'), async (req, res, next) => {
    const { id } = req.params
    const { name, position} = req.query
    const image = req.file && req.file.filename
    const result = await controller.updateMembers(id, { name, position, image })
    res.json({ success: true, result })
  })

  app.get('/members/get/:id', async (req, res, next) => {
    const { id } = req.params
    const members = await controller.getMembers(id)
    res.json({ success: true, result: members })
  })
app.get('/login', authenticateUser)
  
  app.get('/logout', logout)
  
  app.get('/mypage', isLoggedIn, ( req, res ) => {
    const username = req.user.name
    res.send({success:true, result: 'ok, user '+username+' has access to this page'})
  })

  
  app.get('/login', authenticateUser)
  app.get('/logout', logout)
  app.get('/mypage', isLoggedIn, ( req, res ) => {
    const username = req.user.username
    res.send({success:true, result: 'ok, user '+username+' has access to this page'})
  })
  


  // ERROR
  app.use((err, req, res, next) => {
    console.error(err)
    const message = err.message
    res.status(500).json({ success: false, message })
  })

  // authentication 
  
  app.get('/login', authenticateUser)
  
  app.get('/logout', logout)
  
  app.get('/mypage', isLoggedIn, ( req, res ) => {
    const username = req.user.name
    res.send({success:true, result: 'ok, user '+username+' has access to this page'})
  })





  app.listen(8080, () => console.log('server listening on port 8080'))
}



start();
