import sqlite from 'sqlite'
import SQL from 'sql-template-strings';


const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');

// get all blog lists from database

const getAllBlog = async () => {
    
  let blog = `SELECT * FROM blog`
  const rows = await db.all(blog)
  return rows
   }
// get one blog

const getOneBlog = async (id) => {

const oneBlog =  await db.all(`SELECT * FROM blog WHERE blog_id = ${id}`);
const blog = oneBlog[0]
return blog
}

// add blog to the database which takes props

const newBlog = async (props) => {

  try {
console.log(props);
const { title, description, image, content, date } = props

const result = await db.run(SQL`INSERT INTO blog (title, description, image, content, date) VALUES (${title}, ${description}, ${image}, ${content}, ${date} )`);

const newBlog_id = result.stmt.lastId

return newBlog_id 
}
catch(e)
{
  console.log(e);
}
   
}
//  delete blog from  the database which take an id and delete this specefic record
  const deleteBlog = async (blog_id) => {
    const result = await db.run(SQL`DELETE FROM blog WHERE blog_id = ${blog_id}`);
    if(result.stmt.changes === 0){
      return false
    }
    return true
  
  }
//  update blog in the database which take an and props to update this record
  const updateBlog = async (id, props) => {
  const  { title, description, image, content, date } = props
  const result = await db.run(SQL`UPDATE blog SET title=${title}, description=${description}, image=${image}, content=${content}, date=${date} WHERE blog_id = ${id}`);
  if(result.stmt.changes === 0){
    return false
  }
  return true
}

/*EDUCATORS*/

// get all EDUS lists from database

const getAllEdu = async () => {
    
  let edu = `SELECT * FROM educators`
  const rows = await db.all(edu)
  return rows
   }
   //get one edu
   const getOneEdu= async (id) => {

    const edu =  await db.all(`SELECT * FROM educators WHERE educator_id = ${id}`);
    const oneEdu = edu[0]
    return oneEdu
    }
// // add Educatore
const newEdu = async (props) => {
  const { name, about, image} = props
  const result = await db.run(SQL`INSERT INTO educators (name, about, image) VALUES (${name}, ${about}, ${image})`);
  const newEdu_id = result.stmt.lastID
  return newEdu_id 
  }
// Delete Educator
const deleteEdu = async (edu_id) => {
  const result = await db.run(SQL`DELETE FROM educators WHERE educator_id = ${edu_id}`);
  if(result.stmt.changes === 0){
    return false
  }
  return true

}
// Update Educator
const updateEdu = async (id, props) => {
  const  { name, about, image } = props
  const result = await db.run(SQL`UPDATE educators SET name=${name}, about=${about}, image=${image} WHERE educator_id = ${id}`);
  if(result.stmt.changes === 0){
    return false
  }
  return true
}
/*STUDENTS */
const getAllstudents = async () => {
  let returnString = ""
  const rows = await db.all("SELECT * FROM students")
  rows.forEach( ({ student_id, name, image, level, dob }) => returnString+=`[id:${student_id}] - ${name} - ${image} - ${level} - ${dob}` )
  console.log(returnString)
  return rows
}

  const addStudents = async (props) => {
  const { name, image, level, dob } = props
  
  const result = await db.run(SQL`INSERT INTO students (name, image, level, dob) VALUES (${name}, ${image}, ${level}, ${dob})`);
  const students_id = result.stmt.lastID
  return students_id
}

const  deleteStudents = async (student_id) => {
  const result = await db.run(SQL`DELETE FROM students WHERE student_id = ${student_id}`);
  if(result.stmt.changes === 0){
    return false
  }
  return true
}
const updateStudents = async (id, props) => {
  const { name, image, level, dob } = props
  const result = await db.run(SQL`UPDATE students SET image=${image}, level=${level}, dob=${dob} WHERE student_id = ${id}`);
  if(result.stmt.changes === 0){
    return false
  }
  return true
}

const getStudents = async (id) => {
  const Allstudents = await db.all(SQL`SELECT student_id AS id, name, image, level, dob FROM students WHERE student_id = ${id}`);
  const students = Allstudents[0]
  return students
}

/*MEMBERS*/ 

const getAllmembers = async () => {
  let returnString = ""
  const rows = await db.all("SELECT * FROM members")
  rows.forEach( ({ member_id, name, position, image }) => returnString+=`[id:${member_id}] - ${name} - ${position} - ${image} ` )
  return rows
}

const addMembers = async (props) => {
  const { name, position, image } = props
  const result = await db.run(SQL`INSERT INTO members (name, position, image) VALUES (${name}, ${position}, ${image})`);
  const members_id = result.stmt.lastID
  return members_id
}

const  deleteMembers = async (member_id) => {
  const result = await db.run(SQL`DELETE FROM members WHERE member_id = ${member_id}`);
  if(result.stmt.changes === 0){
    return false
  }
  return true
}
const updateMembers = async (id, props) => {
  const { name, position, image} = props
  console.log(props, id)
  const result = await db.run(SQL`UPDATE members SET name=${name}, image=${image}, position=${position} WHERE member_id = ${id}`);
  if(result.stmt.changes === 0){
    return false
  }
  return true
}

const getMembers = async (id) => {
  const Allmembers = await db.all(SQL`SELECT member_id AS id, name, position, image FROM members WHERE member_id = ${id}`);
  const members = Allmembers[0]
  return members
}








  const controller = {
    getAllBlog ,getOneBlog ,newBlog ,deleteBlog ,updateBlog,
    getAllEdu, getOneEdu,newEdu, deleteEdu, updateEdu,
    getAllstudents,
   addStudents,
   deleteStudents,
   updateStudents,
   getStudents,
   getAllmembers,
   addMembers,
   deleteMembers,
   updateMembers,
   getMembers
  }

  return controller
}
export default initializeDatabase
